/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d;

import java.awt.Color;
import text3d.Display.ColorTuple;
import text3d.Display.TextDisplay;
import text3d.ECS.Blueprint;
import text3d.ECS.Components.Colliders.Hitcircle;
import static text3d.ECS.Components.Colliders.Hitcircle.MASK_NEUTRAL;
import text3d.ECS.Components.Location;
import text3d.ECS.Components.Images.TextImage;
import text3d.ECS.ECSHandler;
import text3d.ECS.Entity;
import text3d.ECS.Player.Player;
import text3d.ECS.Systems.BlockHandlerSystem;
import text3d.ECS.Systems.DisplaySystem;
import text3d.ECS.Systems.KeySystem;
import text3d.ECS.Space.Block;
import static text3d.ECS.Space.Block.BLOCK_SIZE;
import static text3d.ECS.Space.Block.EAST;
import static text3d.ECS.Space.Block.NORTH;
import static text3d.ECS.Space.Block.SOUTH;
import static text3d.ECS.Space.Block.WEST;
import text3d.ECS.Space.World;
import text3d.Utility.ImageLoader;
import text3d.Utility.ImageManipulator;
import static text3d.Utility.SuperRandom.oRan;
import text3d.Utility.TextListener;

/**
 *
 * @author FF6EB4
 */
public class Launcher {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        testC();
    }
    
    public static void testC(){
        TextDisplay TD = TextDisplay.Display;
        Blueprint.loadPrints();
        
        
        int[][] map = new int[64][32];
        map[4][4] = 1;
        map[5][5] = 1;
        map[4][5] = 1;
        map[5][4] = 1;
        
        TD.currentBlock = new Block(map);
        World.blocks.add(TD.currentBlock);
        
        ECSHandler.buildSystems();
        ECSHandler.checkEntity(Player.thePlayer);
        ECSHandler.checkEntity(TD.currentBlock);
        
        TD.currentBlock.checkEntity(Player.thePlayer);
        
        Loop loop = new Loop();
    }
    
    public static void testB(){
        TextDisplay TD = TextDisplay.Display;
        TD.currentBlock = new Block();
        World.blocks.add(TD.currentBlock);
        
        ECSHandler.buildSystems();
        ECSHandler.checkEntity(Player.thePlayer);
        ECSHandler.checkEntity(TD.currentBlock);
        
        
        
        ImageLoader.switchMap("GREYSCALE");

        Blueprint.loadPrints();
        Entity test = ECSHandler.build(Blueprint.prints.get("shadow"));
        Entity cube = ECSHandler.build(Blueprint.prints.get("cube"));

        TD.currentBlock.checkEntity(test);
        ECSHandler.checkEntity(test);
        
        TD.currentBlock.checkEntity(cube);
        ECSHandler.checkEntity(cube);
        
        TD.currentBlock.checkEntity(Player.thePlayer);
        
        /*
        ECSHandler.recycle(test);
        
        test = ECSHandler.build(Blueprint.prints.get("shadow"));
        TD.currentBlock.checkEntity(test);
        ECSHandler.checkEntity(test);
        */
        
        Loop loop = new Loop();
    }
    
}
