/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.Display;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.io.File;
import java.util.concurrent.locks.ReentrantLock;
import javax.swing.JFrame;
import javax.swing.JPanel;
import text3d.ECS.Space.Block;
import text3d.Utility.TextListener;
import text3d.Utility.TextMouseListener;

/**
 *
 * @author FF6EB4
 */
public class TextDisplay extends JFrame {
    public static TextDisplay Display = new TextDisplay();
    public static JPanel Panel;
    
    public static final int SCREEN_AREA_X = 150;
    public static final int SCREEN_AREA_Y = 50;
    
    //This marks the center of the screen. It moves around.
    public static Point location = new Point(0,0);
    public static Block currentBlock;
    
    public static Color Background = new Color(14730037);
    
    private static final Point P_ZERO = new Point(0,0);
    
    public static long frame = 0;
    
    public static ReentrantLock lock = new ReentrantLock();
    
    private TextDisplay(){
        super("TextDisplay");
        setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE);
        
        Panel = new JPanel(){
            public void paintComponent(Graphics g){
                super.paintComponent(g);
                TextDisplay.jpPaint(g);
            }
        };
        
        try{
            File font_file = new File("resources/font.ttf");
            Font font = Font.createFont(Font.TRUETYPE_FONT, font_file);
            Font biggerFont = font.deriveFont((float)16);
            Font bolderFont = biggerFont.deriveFont(Font.BOLD);
            
            Panel.setFont(bolderFont);
        }catch (Exception E){
            System.out.println("Failed to load font");
        }
        add(Panel);
        pack();
        setVisible(true);
        
        setSize( 1000, 1000 );
        
        TextListener T = new TextListener();
        addKeyListener(T);
        TextMouseListener TM = new TextMouseListener();
        addMouseListener(TM);
    }
    
    /*
    * This is called from within the JPanel, every time it draws.
    *
    */
    public static void jpPaint(Graphics g){
        double xSize = (double)Panel.getWidth()/(double)SCREEN_AREA_X;
        double ySize = (double)Panel.getHeight()/(double)SCREEN_AREA_Y;
        
        Graphics2D g2 = (Graphics2D)g;
        g2.setColor(Background);
        g2.fillRect(0,0,Panel.getWidth(),Panel.getHeight());
        
        if(currentBlock != null){
            //try{
            currentBlock.draw(g,location, P_ZERO);
            //} catch (NullPointerException E){
            //    System.err.println(E);
            //}
        }
        
        frame++;
    }
    
    public static boolean onScreen(Point P){
        boolean xTest = Math.abs(P.x - location.x) < SCREEN_AREA_X * 2;
        boolean yTest = Math.abs(P.y - location.y) < SCREEN_AREA_Y * 2;
        
        return xTest && yTest;
        
        //return true;
    }
}
