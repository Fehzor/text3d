/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package text3d.Utility;

import java.awt.Color;
import java.util.*;
import java.io.*;
import text3d.Display.ColorTuple;
import text3d.ECS.Components.Images.TextImage;

/**
 *
 * @author FF6EB4
 */
public class ImageLoader {
    private static HashMap<Character,ColorTuple> map;
    private static HashMap<String,HashMap<Character,ColorTuple>> colorKeys;
    private static HashMap<String,TextImage> loadedImages;
    private static ImageLoader me = new ImageLoader();//Force the constructor to do stuff.
    
    
    
    private ImageLoader(){
        loadedImages = new HashMap<>();
        colorKeys = new HashMap<>();
        loadKeys();
        //maptest();
    }
    
    public static HashMap<String,HashMap<Character,ColorTuple>> getColorKeys(){
        return colorKeys;
    }
    
    public static TextImage loadImage(String location){
        if(loadedImages.containsKey(location)){
            return loadedImages.get(location).clone();
        }
        
        ArrayList<ArrayList<ColorTuple>> ret = new ArrayList<>();
        
        try{
            Scanner oScan = new Scanner(new File(location));
            int cols = oScan.nextInt();
            int rows = oScan.nextInt();
            int xO = oScan.nextInt();
            int yO = oScan.nextInt();
            
            oScan.nextLine();
            
            for(int i = 0; i<rows; ++i){
                String row = oScan.nextLine();
                //System.out.println(row+" : "+i);
                ret.add(new ArrayList<ColorTuple>());
                for(int j = 0; j<cols; ++j){
                    char c = row.charAt(j);
                    
                    ColorTuple ct = new ColorTuple(Color.BLACK,Color.BLACK,c);
                    
                    ret.get(i).add(ct);
                    //System.out.print(c);
                }
            }
            
            for(int i = 0; i<rows; ++i){
                String row = oScan.nextLine();
                //System.out.println(row+" : "+i);
                for(int j = 0; j<cols; ++j){
                    //System.out.println(row.charAt(j));
                    ColorTuple ct = map.get(row.charAt(j));
                    //System.out.print(""+ct.icon);
                    ret.get(i).get(j).primary = ct.primary;
                    ret.get(i).get(j).secondary = ct.secondary;
                }
            }
            
            //System.out.println("RETURNING IMAGE");
            TextImage tibret = new TextImage(ret,xO,yO);
            loadedImages.put(location, tibret);
            return tibret;
            
        } catch(FileNotFoundException e){
            System.out.println("FILE NOT FOUND");
            return null;
        } catch(Exception e){
            System.out.println("OTHER ERROR");
            System.err.println(e);
            //map = new HashMap<>();
            return null;
        } 
    }
    
    public static void addMap(String name, HashMap<Character,ColorTuple> add){
        map = add;
        
        colorKeys.put(name, add);
    }
    
    public static ArrayList<ColorTuple> loadMap(String location){
        map = new HashMap<>();
        ArrayList<ColorTuple> ret = new ArrayList<>();
        
        try{
            Scanner oScan = new Scanner(new File(location));
            String key = oScan.next();
            //System.out.println(key);
            int load = oScan.nextInt();
            //System.out.println(load);
            
            for(int i = 0; i<load; ++i){
                ///oScan.nextLine();
                String next = oScan.next();
                //System.out.print(next);
                Color a = new Color(oScan.nextInt(),oScan.nextInt(),oScan.nextInt());
                //System.out.println("Color 1 done");
                Color b = new Color(oScan.nextInt(),oScan.nextInt(),oScan.nextInt());
                //System.out.println("Color 2 done");
                ColorTuple CTAdd = new ColorTuple(a,b,' ');
                map.put(next.charAt(0), CTAdd);
                ret.add(CTAdd);
            }
            
            //ALL maps have - as clear.
            ColorTuple clear = new ColorTuple(ColorTuple.TRANSPARENT,ColorTuple.TRANSPARENT,' ');
            map.put('-',clear);
            
            //System.out.println("Placed -");
            colorKeys.put(key,map);
            
            //System.out.println("MAP LOADED");
            return ret;
        } catch(FileNotFoundException e){
            System.out.println("MAP FILE NOT FOUND");
            return ret;
        } catch(Exception e){
            System.out.println("MAP ERR");
            map = new HashMap<>();
            return ret;
        }
        
        //return ret;
    }
    
    public static void switchMap(String name){
        map = colorKeys.get(name);
    }
    
    public void loadKeys(){
        loadMap("resources/schemes/HUMAN");
        loadMap("resources/schemes/GREYSCALE");
        loadMap("resources/schemes/GREENSCALE");
        loadMap("resources/schemes/GREYSCALE_LIGHT");
        
        //Add additional keys as well. i.e. "GREENSCALE_LIGHT" = greyscale light + green
        ColorTuple Green = new ColorTuple(Color.GREEN,Color.GREEN,' ');
    }
}



//    @&^^^^^^