/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.Utility;

import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.event.*;
import java.util.*;
import static text3d.Display.TextDisplay.Panel;
import static text3d.Display.TextDisplay.SCREEN_AREA_X;
import static text3d.Display.TextDisplay.SCREEN_AREA_Y;

/**
 *
 * @author FF6EB4
 */
public class TextMouseListener implements MouseListener{
    
    public static boolean BUTTON_ONE = false;
    public static boolean BUTTON_TWO = false;
    public static boolean BUTTON_THREE = false;
    
    public static boolean BUTTON_ONE_FIRST = false;
    public static boolean BUTTON_TWO_FIRST = false;
    public static boolean BUTTON_THREE_FIRST = false;
    
    public void mouseClicked(MouseEvent e) {lastMouse = e;}
    public void mouseEntered(MouseEvent e) {lastMouse = e;}
    public void mouseExited(MouseEvent e) {lastMouse = e;}
    
    public static MouseEvent lastMouse = null;

    
    public void mousePressed(MouseEvent e) {
        lastMouse = e;
        if(e.getButton() == MouseEvent.BUTTON1){
            BUTTON_ONE = true;
            BUTTON_ONE_FIRST = true;
        }
        if(e.getButton() == MouseEvent.BUTTON2){
            BUTTON_TWO = true;
            BUTTON_TWO_FIRST = true;
        }
        if(e.getButton() == MouseEvent.BUTTON3){
            BUTTON_THREE = true;
            BUTTON_THREE_FIRST = true;
        }
    }

    public void mouseReleased(MouseEvent e) {
        lastMouse = e;
        if(e.getButton() == MouseEvent.BUTTON1){
            BUTTON_ONE = false;
            BUTTON_ONE_FIRST = false;
        }
        if(e.getButton() == MouseEvent.BUTTON2){
            BUTTON_TWO = false;
            BUTTON_TWO_FIRST = false;
        }
        if(e.getButton() == MouseEvent.BUTTON3){
            BUTTON_THREE = false;
            BUTTON_THREE_FIRST = false;
        }
    }
        
    public static Point mousePos(){
        
        return MouseInfo.getPointerInfo().getLocation();
    }
    
}
