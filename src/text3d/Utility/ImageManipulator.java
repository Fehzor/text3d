/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.Utility;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import text3d.Display.ColorTuple;
import text3d.ECS.Components.Images.TextImage;

/**
 *
 * @author FF6EB4
 */
public class ImageManipulator {
    
    
    private static HashMap<String,HashMap<Character,ColorTuple>> colorKeys;
    
    //USED TO RECOLOR GREY THINGS
    private static ColorTuple clear;
    private static ColorTuple gA;
    private static ColorTuple gB;
    private static ColorTuple gC;
    private static ColorTuple gD;
    
    private ImageManipulator(){
        colorKeys = ImageLoader.getColorKeys();
        
        HashMap<Character,ColorTuple> greyscale = colorKeys.get("GREYSCALE");
        gA = greyscale.get('0');
        gB = greyscale.get('1');
        gC = greyscale.get('2');
        gD = greyscale.get('3');
    }
    
    public static void swapColorScheme(TextImage TIB, HashMap<Character,ColorTuple> newScheme){
        ArrayList<ArrayList<ColorTuple>> img = TIB.getImage();
        
        for(ArrayList<ColorTuple> ACT : img){
            for(ColorTuple CT : ACT){
                ColorTuple temp = CT.clone();
                //System.out.println(CT.equals(temp));
                
                if(CT.equals(gA)){
                    temp = newScheme.get('0');
                }
                if(CT.equals(gB)){
                    temp = newScheme.get('1');
                }
                if(CT.equals(gC)){
                    temp = newScheme.get('2');
                }
                if(CT.equals(gD)){
                    temp = newScheme.get('3');
                }
                
                CT.primary = temp.primary;
                CT.secondary = temp.secondary;
            }
        }
        TIB.setImage(img);
    }
    
    //Builds a color map out of 4 colortuples, and puts it into the ImageLoader.
    public static HashMap<Character,ColorTuple> buildColorMap( ColorTuple A, ColorTuple B, ColorTuple C, ColorTuple D){
        HashMap<Character,ColorTuple> newmap = new HashMap<>();
        newmap.put('0',A);
        newmap.put('1',B);
        newmap.put('2',C);
        newmap.put('3',D);
        
        ColorTuple clear = new ColorTuple(ColorTuple.TRANSPARENT,ColorTuple.TRANSPARENT,' ');
        newmap.put('-',clear);
        
        return newmap;
    }
    
    public static Color mergeColors(Color a, Color b){
        int aRed = a.getRed();
        int aBlu = a.getBlue();
        int aGrn = a.getGreen();
        
        int bRed = b.getRed();
        int bBlu = b.getBlue();
        int bGrn = b.getGreen();
        
        int red = (int)(((float)(aRed + bRed))/2.0);
        int blu = (int)(((float)(aBlu + bBlu))/2.0);
        int grn = (int)(((float)(aGrn + bGrn))/2.0);
        
        return new Color(red,grn,blu);
    }
    
    public static void colorMergeImage(TextImage TIB, ColorTuple merge){
        ArrayList<ArrayList<ColorTuple>> TIBList = TIB.getImage();
        
        for(int i = 0; i<TIBList.size(); ++i){
            for( ColorTuple CT : TIBList.get(i)){
                if(CT.primary != ColorTuple.TRANSPARENT){
                    CT.primary = mergeColors(CT.primary,merge.primary);
                    CT.secondary = mergeColors(CT.secondary,merge.secondary);
                }
            }
        }
    }
    
    public static TextImage resizeImage(TextImage TIB, int width, int height){
        ArrayList<ArrayList<ColorTuple>> retList = new ArrayList<>();
        
        ArrayList<ArrayList<ColorTuple>> base = TIB.getImage();
        
        double iScale = base.get(0).size();
        double jScale = base.size();
        //I got them backwards... my bad
        iScale = (double)iScale / (double)width;
        jScale = (double)jScale / (double)height;
        
        System.out.println(iScale +" "+ jScale);
        
        for(int i = 0; i < height; ++i){
            retList.add(new ArrayList<>());
            for(int j = 0; j < width; ++j){
                ColorTuple CT;
                
               
                int x = (int)Math.floor(i*jScale);
                int y = (int)Math.floor(j*iScale);
                if(x > base.size() - 1){
                    x = base.size() - 1;
                }
                if(y > base.get(0).size() - 1){
                    y = base.get(0).size() - 1;
                }
                
                //if(x < 1) x = 1;
                //if(y < 1) y = 1;
                
                CT = base.get(x).get(y);
                
                retList.get(i).add(CT);
            }
        }
        
        TextImage ret = new TextImage();
        ret.center = new Point((int)(ret.image.get(0).size() / 2),(int)(ret.image.size() / 2));
        ret.setImage(retList);
        
        return ret;
    }
}
