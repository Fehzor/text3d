/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d;

import java.util.ArrayList;
import text3d.ECS.ECSHandler;
import text3d.ECS.SubSystem;
import text3d.ECS.Systems.*;
import text3d.ECS.Space.Block;

/**
 *
 * @author FF6EB4
 */
public class Loop extends Thread{
    public int delay = 100;
    
    public Loop(){
        this.start();
    }
    
    public void run() {
        while(true){
            ECSHandler.runSystems();
            try{
                Thread.sleep(delay);
            } catch(Exception E){};
        }
    }
}
