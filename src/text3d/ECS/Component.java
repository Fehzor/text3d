/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.ECS;

/**
 *
 * @author FF6EB4
 */
public class Component {
    String name;
    
    public boolean reuseable = true;
    
    public Component(String s){
        this.name = s;
    }
    
    public Component clone(){
        Component ret = new Component(this.name);
        ret.set(this);
        return ret;
    }
    
    public void set(Component C){
        this.name = C.name;
        this.reuseable = C.reuseable;
    }
}
