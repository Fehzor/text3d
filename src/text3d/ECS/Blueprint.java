/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.ECS;

import text3d.ECS.Blueprints.Enemies.ShadowPrint;
import java.util.ArrayList;
import java.util.HashMap;
import text3d.ECS.Blueprints.Cubes.BasicCube;
import text3d.ECS.Player.Weapons.Obj.TestBullet;

/**
 *
 * @author FF6EB4
 */
public class Blueprint {
    public ArrayList<Component> components = new ArrayList<>();
    
    
    
    public static HashMap<String,Blueprint> prints;
    
    public static void loadPrints(){
        prints = new HashMap<>();
        
        prints.put("shadow",new ShadowPrint());
        prints.put("cube",new BasicCube());
        prints.put("testbullet",new TestBullet());
        
    }
}
