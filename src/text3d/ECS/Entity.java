/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.ECS;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author FF6EB4
 */
public class Entity {
    private HashMap<String, Component> components;
    
    public Entity(){
        components = new HashMap<>();
    }
    
    public Component getComponent(String comp){
        if(components.containsKey(comp)){
            return components.get(comp);
        }
        return null;
    }
    
    public void addComponent(Component C){
        this.components.put(C.name, C);
    }
    
    public ArrayList<Component> stripComponents(){
        ArrayList<Component> ret = new ArrayList<>();
        
        for(String S : components.keySet()){
            ret.add(components.get(S));
        }
        components = new HashMap<>();
        
        return ret;
    }
}
