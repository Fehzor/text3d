/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.ECS;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import text3d.Display.TextDisplay;
import text3d.ECS.Components.Angle;
import text3d.ECS.Components.Images.Image;
import text3d.ECS.Space.World;
import text3d.ECS.Systems.AISystem;
import text3d.ECS.Systems.AccelerationSystem;
import text3d.ECS.Systems.PlayerAngleSystem;
import text3d.ECS.Systems.BlockHandlerSystem;
import text3d.ECS.Systems.DisplaySystem;
import text3d.ECS.Systems.HitSystem;
import text3d.ECS.Systems.KeySystem;
import text3d.ECS.Systems.VelocitySystem;

/**a
 *
 * @author FF6EB4
 */
public class ECSHandler {
    
    private static ArrayList<Entity> unusedEntities = new ArrayList<>();
    private static HashMap<String,ArrayList<Component>> unusedComponents = new HashMap<String,ArrayList<Component>>();
    
    private static ArrayList<SubSystem> systems = new ArrayList<>();
    private static ArrayList<Entity> entities = new ArrayList<>();
    private static ArrayList<Entity> removeList = new ArrayList<>();
    
    public static void recycle(Entity E){
        removeList.add(E);
        System.out.println("Recycling...");
    }
    
    //Removes an entity from the world.
    private static void deconstruct(Entity E){
        
        World.remove(E);
        
        for(SubSystem S : systems){
            S.removeEntity(E);
        }
        

        unusedEntities.add(E);
        ArrayList<Component> comps = E.stripComponents();

        for(Component C : comps){
            if(C.reuseable){
                if(unusedComponents.containsKey(C.name)){
                    unusedComponents.get(C.name).add(C);
                } else {
                    unusedComponents.put(C.name, new ArrayList<>());
                    unusedComponents.get(C.name).add(C);
                }
            }
        }
    }
    
    public static Entity build(Blueprint B){
        Entity ret;
        if(unusedEntities.size() > 0){
            ret = unusedEntities.remove(0);
        } else {
            ret = new Entity();
        }
        for(Component C : B.components){
            if(C.reuseable && unusedComponents.containsKey(C.name) && unusedComponents.get(C.name).size() > 0){
                Component add = unusedComponents.get(C.name).remove(0);
                add.set(C);
                ret.addComponent(add);
                if(add.name.equals("image")){
                    ((Image)add).a = new Angle();
                    ret.addComponent(((Image)add).a);
                }
            } else {
                Component add = C.clone();
                if(add.name.equals("image")){
                    ((Image)add).a = new Angle();
                    ret.addComponent(((Image)add).a);
                }
                ret.addComponent(add);
            }
        }
        return ret;
    }
    
    
    
    public static void runSystems(){
        for(SubSystem S : systems){
            S.runSystem();
        }
        
        while(removeList.size() > 0){
            deconstruct(removeList.remove(0));
            System.out.println("DECONSTRUCTED!!!");
        }
    }
    
    public static void checkEntity(Entity E){
        for(SubSystem S : systems){
            S.checkEntity(E);
        }
        entities.add(E);
    }
    
    public static void checkSystem(SubSystem S){
        for(Entity E : entities){
            S.checkEntity(E);
        }
        systems.add(S);
    }
    
    public static void buildSystems(){
        ECSHandler.checkSystem(new PlayerAngleSystem());
        
        ECSHandler.checkSystem(new AISystem());
        
        ECSHandler.checkSystem(new KeySystem());
        
        ECSHandler.checkSystem(new HitSystem());
        
        ECSHandler.checkSystem(new AccelerationSystem());
        
        ECSHandler.checkSystem(new VelocitySystem());
        
        ECSHandler.checkSystem(new BlockHandlerSystem());
        
        ECSHandler.checkSystem(new DisplaySystem());
        
        
    }
}
