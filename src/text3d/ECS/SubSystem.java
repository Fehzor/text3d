/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.ECS;

import java.util.ArrayList;

/**
 *
 * @author FF6EB4
 */
public class SubSystem {
    public ArrayList<Entity> entities;
    String [] requisites;
    
    public SubSystem(String [] req){
        this.requisites = req;
        entities = new ArrayList<>();
    }
    
    //Adds or removes an entity from this system.
    public void checkEntity(Entity add){
        //Check each string in requisites to see if it doesn't belong.
        for(String S : requisites){
            if(add.getComponent(S) == null){
                //If it doesn't fit remove it.
                if(entities.contains(add)){
                    this.entities.remove(add);
                }
                return;
            }
        }
        //If we already have it don't add it.
        if(entities.contains(add)){
            return;
        }
        
        //It needs adding!
        entities.add(add);
    }
    
    public void removeEntity(Entity rem){
        if(entities.contains(rem)){
            entities.remove(rem);
        }
    }
    
    public void runSystem(){
        //Insert code here :D
    }
}
