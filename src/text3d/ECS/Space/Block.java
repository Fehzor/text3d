/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.ECS.Space;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import static text3d.Display.TextDisplay.SCREEN_AREA_X;
import static text3d.Display.TextDisplay.SCREEN_AREA_Y;
import static text3d.Display.TextDisplay.onScreen;
import text3d.ECS.Blueprint;
import text3d.ECS.Components.Colliders.HitSquare;
import text3d.ECS.Components.Colliders.Hitcircle;
import text3d.ECS.Components.Location;
import text3d.ECS.Components.Images.TextImage;
import text3d.ECS.Components.Velocity;
import text3d.ECS.ECSHandler;
import text3d.ECS.Entity;
import text3d.ECS.Player.Player;
import static text3d.Utility.SuperRandom.oRan;

/**
 *
 * @author FF6EB4
 */
public class Block extends Entity{
    public static final int BLOCK_SIZE = 320;
    
    public static final int NORTH = 0;
    public static final int EAST = 1;
    public static final int SOUTH = 2;
    public static final int WEST = 3;
    
    public static final Point P_NORTH_OFFSET = new Point(0,-1*(BLOCK_SIZE));
    public static final Point P_SOUTH_OFFSET = new Point(0,(BLOCK_SIZE));
    public static final Point P_EAST_OFFSET = new Point((BLOCK_SIZE),0);
    public static final Point P_WEST_OFFSET = new Point(-1*(BLOCK_SIZE),0);
    
    public static final Point P_NORTHEAST_OFFSET = new Point((BLOCK_SIZE),-1*(BLOCK_SIZE));
    public static final Point P_SOUTHEAST_OFFSET = new Point((BLOCK_SIZE),(BLOCK_SIZE));
    public static final Point P_NORTHWEST_OFFSET = new Point(-1*(BLOCK_SIZE),-1*(BLOCK_SIZE));
    public static final Point P_SOUTHWEST_OFFSET = new Point(-1*(BLOCK_SIZE),(BLOCK_SIZE));
    
    public HashMap<Integer,Block> sides;
    
    private ArrayList<Entity> toAdd;
    private ArrayList<Entity> toRemove;
    private ArrayList<Entity> entities;
    private ArrayList<Entity> drawables;
    private ArrayList<Entity> circleColliders;
    private ArrayList<Entity> squareColliders;
    
    //Background colour.
    //public Color back = new Color(oRan.nextInt(255),oRan.nextInt(255),oRan.nextInt(255));
    
    //Used to prevent concurrency issues with the list of drawables
    //public static ReentrantLock drawLock = new ReentrantLock();
    
    public static Comparator depthComparator = new Comparator<Entity>(){
            public int compare(Entity d1, Entity d2){
                int a = ((Location)d1.getComponent("location")).loc.y;
                int b = ((Location)d2.getComponent("location")).loc.y;
                return a - b;
            }
            
            public boolean equal(Entity d1, Entity d2){
                return compare(d1,d2) == 0;
            }
        };
    
    public Block(){
        sides = new HashMap<>(4);
        
        //By default the block is like a circle.
        sides.put(NORTH, this);
        sides.put(EAST, this);
        sides.put(SOUTH, this);
        sides.put(WEST, this);
        
        toAdd = new ArrayList<>();
        drawables = new ArrayList<>();
        entities = new ArrayList<>();
        circleColliders = new ArrayList<>();
        squareColliders = new ArrayList<>();
        toRemove = new ArrayList<>();
        
        this.addComponent(new SpacialComponent());
    }
    
    public Block(int [][] map){
        sides = new HashMap<>(4);
        
        //By default the block is like a circle.
        sides.put(NORTH, this);
        sides.put(EAST, this);
        sides.put(SOUTH, this);
        sides.put(WEST, this);
        
        toAdd = new ArrayList<>();
        drawables = new ArrayList<>();
        entities = new ArrayList<>();
        circleColliders = new ArrayList<>();
        squareColliders = new ArrayList<>();
        toRemove = new ArrayList<>();
        
        this.addComponent(new SpacialComponent());
        
        for(int i = 0; i < 64; ++i){
            for(int j = 0; j < 32; ++j){
                switch(map[i][j]){
                    case 1:
                    Entity cube = ECSHandler.build(Blueprint.prints.get("cube"));
                    this.checkEntity(cube);
                    ECSHandler.checkEntity(cube);
                    ((Location)cube.getComponent("location")).loc.move(j * 10, i * 5);
                }
            }
        }
    }
    
    public void checkEntity(Entity E){
        this.toAdd.add(E);
    }
    
    public void draw(Graphics g, Point origin, Point offset){
        while(toRemove.size() > 0){
            if(entities.contains(toRemove.get(0))){
                entities.remove(toRemove.remove(0));
            } else {
                toRemove.remove(0);
            }
        }

        while(toAdd.size() > 0){
            Entity E = toAdd.remove(0);
            if(E != null){
                this.entities.add(E);
                if(E.getComponent("location") != null){
                    drawables.add(E);
                    if(E.getComponent("circle") != null){
                        circleColliders.add(E);
                    }
                    if(E.getComponent("square") != null){
                        squareColliders.add(E);
                    }
                }
            }
        }

        try{
            Collections.sort(drawables,depthComparator);
        } catch (Exception E){} //If this fails IDC

        drawSprites(g,origin,offset);
    }
    
    private void drawPartial(boolean right, Graphics g, Point origin, Point offset){
        

        double slope = Double.MAX_VALUE;
        
        Point loca = ((Location)Player.thePlayer.getComponent("location")).loc;
        
        if(offset.equals(P_NORTHWEST_OFFSET)){
            if(!(((loca.x)%BLOCK_SIZE) == 0)){
                slope = ((double)(loca.y)%BLOCK_SIZE) / ((double)(loca.x)%BLOCK_SIZE);
            }
        }
        
        if(offset.equals(P_SOUTHWEST_OFFSET)){
            if(!(((loca.x)%BLOCK_SIZE) == 0)){
                slope = ((double)(BLOCK_SIZE-loca.y)%BLOCK_SIZE) / ((double)(loca.x)%BLOCK_SIZE);
            }
        }
        
        if(offset.equals(P_SOUTHEAST_OFFSET)){
            if(!(((loca.x)%BLOCK_SIZE) == 0)){
                slope = ((double)(BLOCK_SIZE-loca.y)%BLOCK_SIZE) / ((double)(BLOCK_SIZE-loca.x)%BLOCK_SIZE);
            }
        }

        if(offset.equals(P_NORTHEAST_OFFSET)){
            if(!(((loca.x)%BLOCK_SIZE) == 0)){
                slope = ((double)(loca.y)%BLOCK_SIZE) / ((double)(BLOCK_SIZE-loca.x)%BLOCK_SIZE);
            }
        }

            
            //System.out.println(slope);
        for(Entity E : drawables){
            Location L = (Location)E.getComponent("location");

            boolean over = false;
            
            if(offset.equals(P_NORTHWEST_OFFSET)){
                over = slope*(BLOCK_SIZE - L.loc.x) <= (BLOCK_SIZE - L.loc.y);
            }
            
            if(offset.equals(P_SOUTHWEST_OFFSET)){
                over = slope*(BLOCK_SIZE - L.loc.x) <= ( L.loc.y);
            }
            
            if(offset.equals(P_SOUTHEAST_OFFSET)){
                over = slope*( L.loc.x) <= ( L.loc.y);
            }
            
            if(offset.equals(P_NORTHEAST_OFFSET)){
                over = slope*( L.loc.x) <= (BLOCK_SIZE - L.loc.y);
            }

            if(onScreen(new Point(-1*(L.loc.x + offset.x),-1*(L.loc.y + offset.y)))){
                if((over && right) || (!over && !right)){
                    TextImage TI = (TextImage)E.getComponent("image");   
                    Point at = new Point(origin.x + L.x() + offset.x, origin.y + L.y() + offset.y);
                    TI.drawBack(g,at);
                    TI.drawFront(g,at);
                }
            }
        }
    }
    
    private void drawSprites( Graphics g, Point origin, Point offset ){
        
        if(offset.x == 0 && offset.y == 0){
            this.sides.get(NORTH).drawSprites(g, origin, P_NORTH_OFFSET);
            this.sides.get(EAST).drawSprites(g, origin, P_EAST_OFFSET);
            this.sides.get(WEST).drawSprites(g, origin, P_WEST_OFFSET);
            
            this.sides.get(NORTH).sides.get(WEST).drawPartial(true,g,origin,P_NORTHWEST_OFFSET);
            this.sides.get(WEST).sides.get(NORTH).drawPartial(false,g,origin,P_NORTHWEST_OFFSET);
            
            this.sides.get(NORTH).sides.get(EAST).drawPartial(true,g,origin,P_NORTHEAST_OFFSET);
            this.sides.get(EAST).sides.get(NORTH).drawPartial(false,g,origin,P_NORTHEAST_OFFSET);
        }
        
        
        for(Entity E : drawables){
            Location L = (Location)E.getComponent("location");
            if(onScreen(new Point(-1*(L.loc.x + offset.x),-1*(L.loc.y + offset.y)))){
                TextImage TI = (TextImage)E.getComponent("image");   
                Point at = new Point(origin.x + L.x() + offset.x, origin.y + L.y() + offset.y);
                TI.drawBack(g,at);
                TI.drawFront(g,at);
            }
        }
        
        //Draw the others.
        if(offset.x == 0 && offset.y == 0){
            
            this.sides.get(SOUTH).drawSprites(g, origin, P_SOUTH_OFFSET);
            
            this.sides.get(SOUTH).sides.get(WEST).drawPartial(true,g,origin,P_SOUTHWEST_OFFSET);
            this.sides.get(WEST).sides.get(NORTH).drawPartial(false,g,origin,P_SOUTHWEST_OFFSET);
            
            this.sides.get(SOUTH).sides.get(EAST).drawPartial(true,g,origin,P_SOUTHEAST_OFFSET);
            this.sides.get(EAST).sides.get(SOUTH).drawPartial(false,g,origin,P_SOUTHEAST_OFFSET);
        }
    }
    

    
    public void transferTo(Entity E, Block other, Point offset){
        this.drawables.remove(E);
        this.entities.remove(E);
        this.circleColliders.remove(E);
        other.checkEntity(E);
            
        if(E.getComponent("location") != null){
            Location set = ((Location)E.getComponent("location"));
            set.loc = new Point(set.loc.x - offset.x,set.loc.y - offset.y);
        }
    }
    
    public void collide(){
        circleCollisions();
        squareCollisions();
    }
    
    public void circleCollisions(){
        for(int i = 0; i < circleColliders.size(); ++i){
            for(int j = i+1; j<circleColliders.size(); ++j){
                Hitcircle a = (Hitcircle)(circleColliders.get(i).getComponent("circle"));
                Hitcircle b = (Hitcircle)(circleColliders.get(j).getComponent("circle"));
                if(a.effects(b)){
                    Location loca = (Location)(circleColliders.get(i).getComponent("location"));
                    Location locb = (Location)(circleColliders.get(j).getComponent("location"));
                    Point Pa = new Point(loca.loc);
                    Point Pb = new Point(locb.loc);
                    try{
                        Velocity V = (Velocity)circleColliders.get(i).getComponent("velocity");
                        Pa.move(Pa.x+V.vel.x, Pa.y+V.vel.y);
                    } catch (Exception E){}
                    
                    try{
                        Velocity V = (Velocity)circleColliders.get(j).getComponent("velocity");
                        Pb.move(Pb.x+V.vel.x, Pb.y+V.vel.y);
                    } catch (Exception E){}

                    double dist = Pa.distance(Pb);

                    if(dist < a.radius+b.radius){
                        //System.out.println("COLLISION DETECTED!");
                        a.collide(circleColliders.get(i), circleColliders.get(j));
                        b.collide(circleColliders.get(j), circleColliders.get(i));
                    }
                }
            }
        }
    }
    
    public void squareCollisions(){
        for(int i = 0; i < squareColliders.size(); ++i){
            HitSquare HS = (HitSquare) squareColliders.get(i).getComponent("square");
            Location L = (Location) squareColliders.get(i).getComponent("location");
            int x = L.loc.x / 10;
            int y = L.loc.y / 5;
            
            for(int j = 0; j < circleColliders.size(); ++j){
                //try{
                Location L2 = (Location) circleColliders.get(j).getComponent("location");
                Velocity V2 = (Velocity) circleColliders.get(j).getComponent("velocity");
                Hitcircle HC = (Hitcircle) circleColliders.get(j).getComponent("circle");
                int x2 = (L2.loc.x+V2.vel.x) / 10;
                int y2 = (L2.loc.y+V2.vel.y) / 5;
                
                //System.out.println("X: "+x+" "+x2);
                //System.out.println("Y: "+y+" "+y2);
                
                if(x2 == x && y2 == y){
                    HS.collide(squareColliders.get(i), circleColliders.get(j));
                    HC.collide(circleColliders.get(j), squareColliders.get(i));
                }
            }
        }
    }

    void remove(Entity E) {
        this.toRemove.add(E);
    }
}

/*
    private void drawFrontPartial(boolean right, Graphics g, Point origin, Point offset){
         

        double slope = Double.MAX_VALUE;
        
        Point loca = ((Location)Player.thePlayer.getComponent("location")).loc;
        
        if(offset.equals(P_NORTHWEST_OFFSET)){
            if(!(((loca.x)%BLOCK_SIZE) == 0)){
                slope = ((double)(loca.y)%BLOCK_SIZE) / ((double)(loca.x)%BLOCK_SIZE);
            }
        }
        
        if(offset.equals(P_SOUTHWEST_OFFSET)){
            if(!(((loca.x)%BLOCK_SIZE) == 0)){
                slope = ((double)(BLOCK_SIZE-loca.y)%BLOCK_SIZE) / ((double)(loca.x)%BLOCK_SIZE);
            }
        }
        
        if(offset.equals(P_SOUTHEAST_OFFSET)){
            if(!(((loca.x)%BLOCK_SIZE) == 0)){
                slope = ((double)(BLOCK_SIZE-loca.y)%BLOCK_SIZE) / ((double)(BLOCK_SIZE-loca.x)%BLOCK_SIZE);
            }
        }

        if(offset.equals(P_NORTHEAST_OFFSET)){
            if(!(((loca.x)%BLOCK_SIZE) == 0)){
                slope = ((double)(loca.y)%BLOCK_SIZE) / ((double)(BLOCK_SIZE-loca.x)%BLOCK_SIZE);
            }
        }

            
            //System.out.println(slope);
        for(Entity E : drawables){
            Location L = (Location)E.getComponent("location");

            boolean over = false;
            
            if(offset.equals(P_NORTHWEST_OFFSET)){
                over = slope*(BLOCK_SIZE - L.loc.x) <= (BLOCK_SIZE - L.loc.y);
            }
            
            if(offset.equals(P_SOUTHWEST_OFFSET)){
                over = slope*(BLOCK_SIZE - L.loc.x) <= ( L.loc.y);
            }
            
            if(offset.equals(P_SOUTHEAST_OFFSET)){
                over = slope*( L.loc.x) <= ( L.loc.y);
            }
            
            if(offset.equals(P_NORTHEAST_OFFSET)){
                over = slope*( L.loc.x) <= (BLOCK_SIZE - L.loc.y);
            }

            if(onScreen(new Point(-1*(L.loc.x + offset.x),-1*(L.loc.y + offset.y)))){
                if((over && right) || (!over && !right)){
                    TextImage TI = (TextImage)E.getComponent("image");   
                    Point at = new Point(origin.x + L.x() + offset.x, origin.y + L.y() + offset.y);
                    TI.drawFront(g,at);
                }
            }
        }
    }
    */

    /*
    private void drawFront( Graphics g, Point origin, Point offset ){
        for(Entity E : drawables){
            Location L = (Location)E.getComponent("location");
            if(onScreen(new Point(-1*(L.loc.x + offset.x),-1*(L.loc.y + offset.y)))){
                TextImage TI = (TextImage)E.getComponent("image");   
                Point at = new Point(origin.x + L.x() + offset.x, origin.y + L.y() + offset.y);
                TI.drawFront(g,at);
            }
        }
        
        //Draw the others.
        if(offset.x == 0 && offset.y == 0){
            this.sides.get(EAST).drawFront(g, origin, P_EAST_OFFSET);
            this.sides.get(WEST).drawFront(g, origin, P_WEST_OFFSET);
            this.sides.get(SOUTH).drawFront(g, origin, P_SOUTH_OFFSET);
            this.sides.get(NORTH).drawFront(g, origin, P_NORTH_OFFSET);
            
            this.sides.get(NORTH).sides.get(WEST).drawFrontPartial(true,g,origin,P_NORTHWEST_OFFSET);
            this.sides.get(WEST).sides.get(NORTH).drawFrontPartial(false,g,origin,P_NORTHWEST_OFFSET);
            
            this.sides.get(NORTH).sides.get(EAST).drawFrontPartial(true,g,origin,P_NORTHEAST_OFFSET);
            this.sides.get(EAST).sides.get(NORTH).drawFrontPartial(false,g,origin,P_NORTHEAST_OFFSET);
            
            this.sides.get(SOUTH).sides.get(WEST).drawFrontPartial(true,g,origin,P_SOUTHWEST_OFFSET);
            this.sides.get(WEST).sides.get(SOUTH).drawFrontPartial(false,g,origin,P_SOUTHWEST_OFFSET);
            
            this.sides.get(SOUTH).sides.get(EAST).drawFrontPartial(true,g,origin,P_SOUTHEAST_OFFSET);
            this.sides.get(EAST).sides.get(SOUTH).drawFrontPartial(false,g,origin,P_SOUTHEAST_OFFSET);
        }
    }
*/