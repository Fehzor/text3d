/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.ECS.Systems;

import java.awt.Point;
import static java.awt.event.KeyEvent.*;
import text3d.Display.TextDisplay;
import static text3d.Display.TextDisplay.Panel;
import static text3d.Display.TextDisplay.SCREEN_AREA_X;
import static text3d.Display.TextDisplay.SCREEN_AREA_Y;
import static text3d.Utility.TextListener.isHeld;
import text3d.ECS.*;
import text3d.ECS.Components.AI;
import text3d.ECS.Components.Angle;
import text3d.ECS.Components.Location;
import text3d.Utility.TextMouseListener;

/**
 *
 * @author FF6EB4
 */


//
//  FOR NOW THIS WILL MOVE THE SCREEN ONLY.
//
public class AISystem extends SubSystem{
    
    public AISystem(){
        super(new String[]{"intel"});
    }
    
    public void runSystem(){
        for(Entity E : entities){
            AI intel = (AI)E.getComponent("intel");
            intel.act(E);
        }
    }
}
