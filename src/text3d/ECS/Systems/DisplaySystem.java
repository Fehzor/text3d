/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.ECS.Systems;

import java.awt.Point;
import static java.awt.event.KeyEvent.*;
import text3d.Display.TextDisplay;
import static text3d.Display.TextDisplay.SCREEN_AREA_X;
import static text3d.Display.TextDisplay.SCREEN_AREA_Y;
import static text3d.Utility.TextListener.isHeld;
import text3d.ECS.*;
import text3d.ECS.Components.Location;

/**
 *
 * @author FF6EB4
 */


//
//  FOR NOW THIS WILL MOVE THE SCREEN ONLY.
//
public class DisplaySystem extends SubSystem{
    
    public DisplaySystem(){
        super(new String[]{"camera","location"});
    }
    
    public void runSystem(){
        if(entities.size() == 0){
            return;
        }
        Location loc = (Location)entities.get(0).getComponent("location");
        
        TextDisplay.location = new Point(loc.loc);
        
        TextDisplay.location.setLocation(-1*TextDisplay.location.x + SCREEN_AREA_X / 2, -1*TextDisplay.location.y + SCREEN_AREA_Y / 2);
        
        TextDisplay.Display.repaint();
    }
}
