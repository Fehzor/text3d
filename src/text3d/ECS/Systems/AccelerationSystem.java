/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.ECS.Systems;

import java.awt.Point;
import static java.awt.event.KeyEvent.*;
import text3d.Display.TextDisplay;
import static text3d.Utility.TextListener.isHeld;
import text3d.ECS.*;
import text3d.ECS.Components.Acceleration;
import text3d.ECS.Components.Images.TextImageAnimated;
import text3d.ECS.Components.Images.TextImageDualPaper;
import text3d.ECS.Components.Location;
import text3d.ECS.Components.Velocity;

/**
 *
 * @author FF6EB4
 */


//
//  FOR NOW THIS WILL MOVE THE SCREEN ONLY.
//
public class AccelerationSystem extends SubSystem{
    
    public AccelerationSystem(){
        super(new String[]{"acceleration","velocity"});
    }
    
    public void runSystem(){
        for(Entity E : entities){
            Velocity velo = (Velocity)E.getComponent("velocity");
            Acceleration acc = (Acceleration)E.getComponent("acceleration");
            
            velo.vel.move(acc.acc.x + velo.vel.x, acc.acc.y + velo.vel.y);
            
        }
    }
}
