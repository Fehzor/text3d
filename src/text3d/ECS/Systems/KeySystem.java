/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.ECS.Systems;

import text3d.ECS.Player.Player;
import static java.awt.event.KeyEvent.*;
import text3d.Display.TextDisplay;
import static text3d.Utility.TextListener.isHeld;
import text3d.ECS.*;
import text3d.ECS.Components.Location;
import text3d.ECS.Components.Velocity;
import static text3d.Utility.TextMouseListener.BUTTON_ONE;
import static text3d.Utility.TextMouseListener.BUTTON_ONE_FIRST;

/**
 *
 * @author FF6EB4
 */


//
//  FOR NOW THIS WILL MOVE THE SCREEN ONLY.
//
public class KeySystem extends SubSystem{
    
    public KeySystem(){
        super(new String[]{"NONE"});
    }
    
    public void runSystem(){
        Velocity vel = (Velocity)Player.thePlayer.getComponent("velocity");
        //System.out.println(loc);
        if(isHeld(VK_A)){
            vel.vel.move(vel.vel.x-3, vel.vel.y);
        }
        if(isHeld(VK_S)){
            vel.vel.move(vel.vel.x, vel.vel.y+3);
        }
        if(isHeld(VK_D)){
            vel.vel.move(vel.vel.x+3, vel.vel.y);
        }
        if(isHeld(VK_W)){
            vel.vel.move(vel.vel.x, vel.vel.y-3);
        }
        
        if(BUTTON_ONE_FIRST){
            BUTTON_ONE_FIRST = false;
            Player.thePlayer.current.act();
        }
    }
}
