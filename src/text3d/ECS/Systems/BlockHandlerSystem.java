/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.ECS.Systems;

import java.awt.Point;
import text3d.Display.TextDisplay;
import static text3d.Display.TextDisplay.currentBlock;
import text3d.ECS.Components.Location;
import text3d.ECS.Entity;
import text3d.ECS.SubSystem;
import static text3d.ECS.Space.Block.BLOCK_SIZE;
import static text3d.ECS.Space.Block.EAST;
import static text3d.ECS.Space.Block.NORTH;
import static text3d.ECS.Space.Block.P_EAST_OFFSET;
import static text3d.ECS.Space.Block.P_NORTH_OFFSET;
import static text3d.ECS.Space.Block.P_SOUTH_OFFSET;
import static text3d.ECS.Space.Block.P_WEST_OFFSET;
import static text3d.ECS.Space.Block.SOUTH;
import static text3d.ECS.Space.Block.WEST;

/**
 *
 * @author FF6EB4
 */
public class BlockHandlerSystem extends SubSystem{
    public BlockHandlerSystem(){
        super(new String[]{"location"});
    }
    
    public void runSystem(){
        if(entities.size() == 0){
            return;
        }
        for(Entity E : entities){
            Point loc = ((Location)E.getComponent("location")).loc;


            if(loc.x < 0){
                currentBlock.transferTo(E, currentBlock.sides.get(WEST), P_WEST_OFFSET);
                
                if(E.getComponent("player") != null){
                    currentBlock = currentBlock.sides.get(WEST);
                }
            }

            if(loc.y < 0){
                currentBlock.transferTo(E, currentBlock.sides.get(NORTH), P_NORTH_OFFSET);
                
                if(E.getComponent("player") != null){
                    currentBlock = currentBlock.sides.get(NORTH);
                }
            }

            if(loc.x > BLOCK_SIZE){
                currentBlock.transferTo(E, currentBlock.sides.get(EAST), P_EAST_OFFSET);
                
                if(E.getComponent("player") != null){
                    currentBlock = currentBlock.sides.get(EAST);
                }
            }

            if(loc.y > BLOCK_SIZE){
                currentBlock.transferTo(E, currentBlock.sides.get(SOUTH), P_SOUTH_OFFSET);
                
                if(E.getComponent("player") != null){
                    currentBlock = currentBlock.sides.get(SOUTH);
                }
            }
        }
        
    }
}
