/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.ECS.Systems;

import java.awt.Point;
import static java.awt.event.KeyEvent.*;
import text3d.Display.TextDisplay;
import static text3d.Utility.TextListener.isHeld;
import text3d.ECS.*;
import text3d.ECS.Components.Images.TextImageAnimated;
import text3d.ECS.Components.Images.TextImageDualPaper;
import text3d.ECS.Components.Location;
import text3d.ECS.Components.Velocity;

/**
 *
 * @author FF6EB4
 */


//
//  FOR NOW THIS WILL MOVE THE SCREEN ONLY.
//
public class VelocitySystem extends SubSystem{
    
    public VelocitySystem(){
        super(new String[]{"velocity","location"});
    }
    
    public void runSystem(){
        for(Entity E : entities){
            Location loc = (Location)E.getComponent("location");
            Velocity vel = (Velocity)E.getComponent("velocity");
            
            loc.loc.move(vel.vel.x + loc.loc.x, vel.vel.y + loc.loc.y);
            
            try{
                TextImageAnimated TIA =(TextImageAnimated)(E.getComponent("image"));
                TIA.stop = vel.vel.x == 0 && vel.vel.y == 0;
                if(TIA.stop)TIA.secondFrame = false;
            } catch (Exception ex){}
            
            try{
                TextImageAnimated TIA =(TextImageAnimated)(((TextImageDualPaper)E.getComponent("image")).set);
                TIA.stop = vel.vel.x == 0 && vel.vel.y == 0;
                if(TIA.stop)TIA.secondFrame = false;
            } catch (Exception ex){}
            
            vel.vel=new Point(0,0);
            
        }
    }
}
