/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.ECS.Systems;

import text3d.ECS.Player.Player;
import java.awt.Point;
import static java.awt.event.KeyEvent.*;
import text3d.Display.TextDisplay;
import static text3d.Display.TextDisplay.Panel;
import static text3d.Display.TextDisplay.SCREEN_AREA_X;
import static text3d.Display.TextDisplay.SCREEN_AREA_Y;
import static text3d.Utility.TextListener.isHeld;
import text3d.ECS.*;
import text3d.ECS.Components.Angle;
import text3d.ECS.Components.Location;
import text3d.Utility.TextMouseListener;

/**
 *
 * @author FF6EB4
 */


//
//  FOR NOW THIS WILL MOVE THE SCREEN ONLY.
//
public class PlayerAngleSystem extends SubSystem{
    
    public PlayerAngleSystem(){
        super(new String[]{"location","angle","player"});
    }
    
    public void runSystem(){
        Angle ang = (Angle)Player.thePlayer.getComponent("angle");
        
        Point mous = TextMouseListener.mousePos();
        double angle = Math.atan2((double)Panel.getWidth()/(double)2 - (double)mous.x, (double)Panel.getHeight()/(double)2 - (double)mous.y);
        if(angle < 0){
            angle += Math.toRadians(360);
        }
        //System.out.println("ANGLE = "+angle);
        ang.setAngle(angle);
    }
}
