/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.ECS.Systems;

import static java.awt.event.KeyEvent.*;
import text3d.Display.TextDisplay;
import static text3d.Utility.TextListener.isHeld;
import text3d.ECS.*;
import text3d.ECS.Components.Colliders.Hitcircle;
import text3d.ECS.Components.Location;
import text3d.ECS.Components.Velocity;
import text3d.ECS.Space.Block;

/**
 *
 * @author FF6EB4
 */


//
//  FOR NOW THIS WILL MOVE THE SCREEN ONLY.
//
public class HitSystem extends SubSystem{
    
    
    public HitSystem(){
        super(new String[]{"space"});
    }
    
    public void runSystem(){
        for(Entity B : entities){
            ((Block)B).collide();
        }
    }
}
