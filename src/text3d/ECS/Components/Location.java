/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.ECS.Components;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import text3d.Display.ColorTuple;
import text3d.ECS.Component;
import text3d.ECS.Player.Player;

/**
 *
 * @author FF6EB4
 */
public class Location extends Component{
    public Point loc;
    
    public Location(int startX, int startY){
        super("location");
        loc = new Point(startX,startY);
    }
    
    public int x(){
        return loc.x;
    }
    
    public int y(){
        return loc.y;
    }
    
    public Location clone(){
        return new Location(loc.x,loc.y);
    }
    
    public void set(Component C){
        super.set(C);
        this.loc.setLocation(((Location)C).loc);
    }
    
    public void setToPlayerLocation(){
        this.set(Player.thePlayer.getComponent("location"));
    }
}
