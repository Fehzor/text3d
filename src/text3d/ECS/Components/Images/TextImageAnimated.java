/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.ECS.Components.Images;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import text3d.Display.ColorTuple;
import static text3d.Display.TextDisplay.frame;
import text3d.ECS.Component;
import text3d.ECS.Components.Angle;

/**
 *
 * Represents a basic text image, always faces the camera.
 * 
 * @author FF6EB4
 */
public class TextImageAnimated extends TextImage{
    
    public TextImage frameOne; // false
    public TextImage frameTwo; // true;
    
    public boolean stop = true;
    public int delay = 3;
    public boolean secondFrame = true;
    
    public TextImageAnimated(){
        super();
    }
    
    public TextImageAnimated clone(){
        TextImageAnimated ret = new TextImageAnimated();
        
        ret.frameOne = this.frameOne.clone();
        ret.frameTwo = this.frameTwo.clone();
        
        ret.center = new Point(this.center);
        
        return ret;
    }
    
    /**
     * Draws this image at a position, x, y.
     * @param g
     * 
     * The Graphics object responsible for drawing these things.
     * 
     * @param x
     * @param y
     * 
     * The x and y coordinates of where it is. In squares, not pixels.
     * The size of a block.
     */
    public void drawBack(Graphics g, Point location){
        if(hidden){
            return;
        }
        drawSubImagesBehind(g,location);
        
        checkFrame();
        if(secondFrame){
            frameTwo.drawBack(g,location);
        } else {
            frameOne.drawBack(g,location);
        }
    }
    
    public void drawFront(Graphics g, Point location){
        if(hidden){
            return;
        }
        if(secondFrame){
            frameTwo.drawFront(g,location);
        } else {
            frameOne.drawFront(g,location);
        }
        
        drawSubImagesFront(g,location);

    }
    
    public void checkFrame(){
        if(!stop && frame % delay == 0){
            secondFrame = !secondFrame;
        }
        
        if(secondFrame){
            this.center = frameTwo.center;
        } else {
            this.center = frameOne.center;
        }
    }
    
    public void drawSubImagesBehind(Graphics g, Point location){
        for(Image I : imageList){
            double ang = (90+angle.get(I)) % Math.toRadians(360);
            if(ang < 0){
                ang += 360;
            }
            if(Math.toDegrees(ang) < 180){
                
                double scale = Math.cos (ang);
                int x = (int)( distance.get(I) *2* scale - distance.get(I));
                try{
                int add = (int)(distance.get(I) - (scale) * (distance.get(I) / 2))/2;
                
                I.drawBack(g, new Point(center.x + add+location.x + x,location.y + height.get(I)));
                I.drawFront(g, new Point(center.x + add+location.x + x,location.y + height.get(I)));
                } catch (Exception E){}
            }
        }
    }
    public void drawSubImagesFront(Graphics g, Point location){
        for(Image I : imageList){
            double ang = (90+angle.get(I)) % Math.toRadians(360);
            if(ang < 0){
                ang += 360;
            }
            if(Math.toDegrees(ang) > 180){
                
                double scale = Math.cos (ang);
                int x = (int)( distance.get(I) *2* scale - distance.get(I));
                
                try{
                int add = (int)(distance.get(I) - (scale) * (distance.get(I) / 2))/2 ;
                I.drawBack(g, new Point(-center.x + add+location.x + x,location.y + height.get(I)));
                I.drawFront(g, new Point(-center.x + add+location.x + x,location.y + height.get(I)));
                } catch (Exception E){}
            }
        }
    }
    
    public ArrayList<ArrayList<ColorTuple>> getImage(){
        checkFrame();
        if(secondFrame){
            return this.frameTwo.getImage();
        } else {
            return this.frameOne.getImage();
        }
    }
}
