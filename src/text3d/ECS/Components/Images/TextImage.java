/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.ECS.Components.Images;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import text3d.Display.ColorTuple;
import text3d.ECS.Component;

/**
 *
 * Represents a basic text image, always faces the camera.
 * 
 * @author FF6EB4
 */
public class TextImage extends Image{
    public ArrayList<ArrayList<ColorTuple>> image;
    public Point center;
    
    public boolean flipped = false;
    
    //TextImages may have images attached to them.
    public ArrayList<Image> imageList = new ArrayList<>();
    public HashMap<Image,Integer> distance = new HashMap<>();
    public HashMap<Image,Integer> height = new HashMap<>();
    public HashMap<Image,Double> angle = new HashMap<>();
    
    public TextImage(){
        super();
        image = new ArrayList<>();
        image.add(new ArrayList<>());
        image.get(0).add(new ColorTuple(Color.BLACK,Color.WHITE,'#'));
        
        center = new Point(0,0);
    }
    
    public void registerSubImage(Image I, int dist, int tall, double ang){
        imageList.add(I);
        distance.put(I, dist);
        this.height.put(I,tall);
        this.angle.put(I,ang);
    }
    
    public TextImage(ArrayList<ArrayList<ColorTuple>> AACT, int X0, int Y0){
        super();
        
        this.image = AACT;
        
        center = new Point(X0,Y0);
    }
    
    public TextImage clone(){
        TextImage ret = new TextImage();
        
        ret.image = new ArrayList<ArrayList<ColorTuple>>();
        
        int i = 0;
        for(ArrayList<ColorTuple> outer : image){
            ret.image.add(new ArrayList<>());
            for(ColorTuple inner : outer){
                ret.image.get(i).add(inner);
            }
            i++;
        }
        
        ret.center = new Point(this.center);
        
        return ret;
    }
    
    /**
     * Draws this image at a position, x, y.
     * @param g
     * 
     * The Graphics object responsible for drawing these things.
     * 
     * @param x
     * @param y
     * 
     * The x and y coordinates of where it is. In squares, not pixels.
     * 
     * @param xSize
     * @param ySize 
     * 
     * The size of a block.
     */
    public void drawBack(Graphics g, Point location){ 
        if(hidden){
            return;
        }
        
        drawSubImagesBehind(g,location);
        
        ColorTuple bigPixel;
        for(int i = 0; i<image.size(); ++i){
            for(int j = 0; j<image.get(i).size();++j){
               //System.out.println(i+", "+j);
                if(flipped){
                   bigPixel = image.get(image.size()-i-1).get(j);
                } else {
                    bigPixel = image.get(i).get(j);
                }
                bigPixel.drawBack(g,j+location.x-center.x,i+location.y-center.y);
            }
        }
    }
    
    public void drawFront(Graphics g, Point location){ 
        if(hidden){
            return;
        }
        ColorTuple bigPixel;
        for(int i = 0; i<image.size(); ++i){
            for(int j = 0; j<image.get(i).size();++j){
               //System.out.println(i+", "+j);
                if(flipped){
                   bigPixel = image.get(image.size()-i-1).get(j);
                } else {
                    bigPixel = image.get(i).get(j);
                }
                bigPixel.drawFront(g,j+location.x-center.x,i+location.y-center.y);
            }
        }
        
        drawSubImagesFront(g,location);
    }
    
    
    public void drawSubImagesBehind(Graphics g, Point location){
        for(Image I : imageList){
            double ang = (90+angle.get(I)) % Math.toRadians(360);
            if(ang < 0){
                ang += 360;
            }
            if(Math.toDegrees(ang) < 180){
                
                double scale = Math.cos (ang);
                int x = (int)( distance.get(I) *2* scale - distance.get(I));
                try{
                int add = (int)(distance.get(I) - (scale) * (distance.get(I) / 2))/2;
                
                I.drawBack(g, new Point(center.x + add+location.x + x,location.y + height.get(I)));
                I.drawFront(g, new Point(center.x + add+location.x + x,location.y + height.get(I)));
                } catch (Exception E){}
            }
        }
    }
    public void drawSubImagesFront(Graphics g, Point location){
        for(Image I : imageList){
            double ang = (90+angle.get(I)) % Math.toRadians(360);
            if(ang < 0){
                ang += 360;
            }
            if(Math.toDegrees(ang) > 180){
                
                double scale = Math.cos (ang);
                int x = (int)( distance.get(I) *2* scale - distance.get(I));
                
                try{
                int add = (int)(distance.get(I) - (scale) * (distance.get(I) / 2))/2 ;
                I.drawBack(g, new Point(-center.x + add+location.x + x,location.y + height.get(I)));
                I.drawFront(g, new Point(-center.x + add+location.x + x,location.y + height.get(I)));
                } catch (Exception E){}
            }
        }
    }
    
    public void setImage(ArrayList<ArrayList<ColorTuple>> set){
        this.image = set;
    }
    
    public ArrayList<ArrayList<ColorTuple>> getImage(){
        return this.image;
    }
}
