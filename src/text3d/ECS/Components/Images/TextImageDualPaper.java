/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.ECS.Components.Images;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import text3d.Display.ColorTuple;
import text3d.ECS.Component;
import text3d.ECS.Components.Angle;
import text3d.Utility.ImageLoader;

/**
 *
 * Represents a basic text image, always faces the camera.
 * 
 * @author FF6EB4
 */
public class TextImageDualPaper extends TextImage{
    public TextImage side = new TextImage();
    public TextImage front = new TextImage();;
    public TextImage back = new TextImage();;
    
    public TextImage set = new TextImage();
    
    public TextImageDualPaper(Angle set, TextImage front, TextImage side, TextImage back){
        super();
        this.a = set;
        this.image = new ArrayList<ArrayList<ColorTuple>>();
        
        this.side = side;
        this.front = front;
        this.back = back;
        this.set = front;
    }
    
    //Constructs a fully animated image using the location of the front image -.txt
    public TextImageDualPaper(Angle a,String location){
        super();
        this.image = new ArrayList<ArrayList<ColorTuple>>();
        this.a = a;
        
        TextImage front = ImageLoader.loadImage(location+"front.txt");
        TextImage frontw = ImageLoader.loadImage(location+"frontwalk.txt");
        TextImage side = ImageLoader.loadImage(location+"side.txt");
        TextImage sidew = ImageLoader.loadImage(location+"sidewalk.txt");
        TextImage back = ImageLoader.loadImage(location+"back.txt");
        TextImage backw = ImageLoader.loadImage(location+"backwalk.txt");
        
        TextImageAnimated fronta = new TextImageAnimated();
        fronta.frameOne = front;
        fronta.frameTwo = frontw;
        TextImageAnimated sidea = new TextImageAnimated();
        sidea.frameOne = side;
        sidea.frameTwo = sidew;
        TextImageAnimated backa = new TextImageAnimated();
        backa.frameOne = back;
        backa.frameTwo = backw;
        
        this.side = sidea;
        this.front = fronta;
        this.back = backa;
    }
    
    
    
    
    public TextImageDualPaper clone(){
        TextImageDualPaper ret = new TextImageDualPaper(a, this.front.clone(), this.side.clone(), this.back.clone());
        
        ret.center = new Point(this.center);
        
        return ret;
    }
    
    /**
     * Draws this image at a position, x, y.
     * @param g
     * 
     * The Graphics object responsible for drawing these things.
     * 
     * @param x
     * @param y
     * 
     * The x and y coordinates of where it is. In squares, not pixels.
     * 
     * @param xSize
     * @param ySize 
     * 
     * The size of a block.
     */
    public void drawBack(Graphics g, Point location){ 
        if(hidden){
            return;
        }
        drawSubImagesBehind(g,location);
        
        double angle = a.angle() % Math.toRadians(360);
        
        double scale = 1 / Math.sin(angle);
        //System.out.println(scale);
        
        int margin = 30;
        
        //Used to center the sprite...
        

        if(angle < Math.toRadians(180+margin) && angle > Math.toRadians(180-margin)){
            //this.image = front.image;
            this.set = front;
            scale = 1 / Math.cos(angle);
            if(scale < 0){
                scale = scale * -1;
            }
        } else if(angle > Math.toRadians(360-margin) || angle < Math.toRadians(margin)){
            //this.image = back.image;
            this.set = back;
            scale = 1 / Math.cos(angle);
            if(scale < 0){
                scale = scale * -1;
            }
        } else {
            //this.image = side.image;
            this.set = side;
            
            if(scale < 0){
                scale = scale * -1;
                flipped = false;
            } else {
                flipped = true;
            }
        }
        
        this.image = set.getImage();
        
        int add = (int)(image.get(0).size() - (1/scale) * (image.get(0).size() / 2))/2;
        
        
        ColorTuple bigPixel;
        for(int i = 0; i<image.size(); ++i){
            for(int j = 0; j<image.get(i).size();++j){
               //System.out.println(i+", "+j);
               try{
                int x = (int)Math.floor(((double)j)*scale);

                if(flipped){
                    //-2 because of the -1 above :D
                    bigPixel = image.get(i).get(image.get(0).size() - x - 1);
                } else {
                    bigPixel = image.get(i).get(x);
                }
                bigPixel.drawBack(g,j+location.x-set.center.x+add,i+location.y-set.center.y);
               }catch (Exception E){}
            }
        }
    }
    
    public void drawFront(Graphics g, Point location){ 
        if(hidden){
            return;
        }
        double angle = a.angle() % Math.toRadians(360);
        
        double scale = 1 / Math.sin(angle);
        //System.out.println(scale);
        
        int margin = 30;
        
        //Used to center the sprite...
        

        if(angle < Math.toRadians(180+margin) && angle > Math.toRadians(180-margin)){
            //this.image = front.image;
            this.set = front;
            scale = 1 / Math.cos(angle);
            if(scale < 0){
                scale = scale * -1;
            }
        } else if(angle > Math.toRadians(360-margin) || angle < Math.toRadians(margin)){
            //this.image = back.image;
            this.set = back;
            scale = 1 / Math.cos(angle);
            if(scale < 0){
                scale = scale * -1;
            }
        } else {
            //this.image = side.image;
            this.set = side;
            
            if(scale < 0){
                scale = scale * -1;
                flipped = false;
            } else {
                flipped = true;
            }
        }
        
        this.image = set.getImage();
        
        int add = (int)(image.get(0).size() - (1/scale) * (image.get(0).size() / 2))/2;
        
        
        ColorTuple bigPixel;
        for(int i = 0; i<image.size(); ++i){
            for(int j = 0; j<image.get(i).size();++j){
               //System.out.println(i+", "+j);
               try{
                int x = (int)Math.floor(((double)j)*scale);

                if(flipped){
                    //-2 because of the -1 above :D
                    bigPixel = image.get(i).get(image.get(0).size() - x - 1);
                } else {
                    bigPixel = image.get(i).get(x);
                }
                bigPixel.drawFront(g,j+location.x-set.center.x+add,i+location.y-set.center.y);
               }catch (Exception E){}
            }
        }
        
        drawSubImagesFront(g, location);
    }
    
    public void drawSubImagesBehind(Graphics g, Point location){
        for(Image I : imageList){
            double ang = (90+angle.get(I)+this.a.angle()) % Math.toRadians(360);
            if(ang < 0){
                ang += 360;
            }
            if(Math.toDegrees(ang) < 180){
                
                double scale = Math.cos (ang);
                int x = (int)( distance.get(I) *2* scale - distance.get(I));
                try{
                int add = (int)(distance.get(I) - (scale) * (distance.get(I) / 2))/2;
                
                I.drawBack(g, new Point(center.x + add+location.x + x,location.y + height.get(I)));
                I.drawFront(g, new Point(center.x + add+location.x + x,location.y + height.get(I)));
                } catch (Exception E){}
            }
        }
    }
    public void drawSubImagesFront(Graphics g, Point location){
        for(Image I : imageList){
            double ang = (90+angle.get(I)+this.a.angle()) % Math.toRadians(360);
            if(ang < 0){
                ang += 360;
            }
            if(Math.toDegrees(ang) > 180){
                
                double scale = Math.cos (ang);
                int x = (int)( distance.get(I) *2* scale - distance.get(I));
                
                try{
                int add = (int)(distance.get(I) - (scale) * (distance.get(I) / 2))/2 ;
                I.drawBack(g, new Point(-center.x + add+location.x + x,location.y + height.get(I)));
                I.drawFront(g, new Point(-center.x + add+location.x + x,location.y + height.get(I)));
                } catch (Exception E){}
            }
        }
    }
}
