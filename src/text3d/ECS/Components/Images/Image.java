/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.ECS.Components.Images;

import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.locks.ReentrantLock;
import text3d.ECS.Component;
import text3d.ECS.Components.Angle;

/**
 *
 * @author FF6EB4
 */
public class Image  extends Component{
        
    public Angle a;
    
    public boolean hidden = false;
    
    public Image(){
        super("image");
        reuseable = false;
    }
    
    public void drawBack(Graphics g, Point location){}
    
    //IF THE IMAGE IS A SPRITE OR SOMETHING USE THIS ONE.
    public void drawFront(Graphics g, Point location){}
}
