/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.ECS.Components.Images;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import text3d.Display.ColorTuple;
import text3d.ECS.Component;

/**
 *
 * Represents a basic text image, always faces the camera.
 * 
 * @author FF6EB4
 */
public class TextImageReuseable extends TextImage{
    public ArrayList<ArrayList<ColorTuple>> image;
    public Point center;
    
    public boolean flipped = false;

    public TextImageReuseable(TextImage base){
        this.image = base.image;
        this.center = base.center;
        reuseable = true;
    }
    
    public TextImageReuseable clone(){
        TextImageReuseable ret = new TextImageReuseable(this);
        
        ret.image = new ArrayList<ArrayList<ColorTuple>>();
        
        int i = 0;
        for(ArrayList<ColorTuple> outer : image){
            ret.image.add(new ArrayList<>());
            for(ColorTuple inner : outer){
                ret.image.get(i).add(inner);
            }
            i++;
        }
        
        ret.center = new Point(this.center);
        
        return ret;
    }
    
    public void set(Component C){
        super.set(C);
        TextImageReuseable TIR = (TextImageReuseable)C;
        
        this.center.setLocation(TIR.center);
        
        this.image = new ArrayList<>();
        
        int i = 0;
        for(ArrayList<ColorTuple> outer : TIR.image){
            this.image.add(new ArrayList<>());
            for(ColorTuple inner : outer){
                this.image.get(i).add(inner);
            }
            i++;
        }
    }
    
}
