/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.ECS.Components;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import text3d.Display.ColorTuple;
import text3d.ECS.Component;

/**
 *
 * @author FF6EB4
 */
public class Velocity extends Component{
    public Point vel;
    
    public Velocity(int startX, int startY){
        super("velocity");
        vel = new Point(startX,startY);
    }
    
    public int x(){
        return vel.x;
    }
    
    public int y(){
        return vel.y;
    }
    
    public Velocity clone(){
        return new Velocity(vel.x,vel.y);
    }
    
    public void set(Component C){
        super.set(C);
        this.vel.setLocation(((Velocity)C).vel);
    }
}
