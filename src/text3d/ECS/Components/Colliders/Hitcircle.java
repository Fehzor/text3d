/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.ECS.Components.Colliders;

import java.awt.Point;
import text3d.ECS.Component;
import text3d.ECS.Components.Location;
import text3d.ECS.Components.Velocity;
import text3d.ECS.Entity;

/**
 *
 * @author FF6EB4
 */
public class Hitcircle extends Component{
    
    public static final int MASK_PLAYER = 0;
    public static final int MASK_ENEMY = 1;
    public static final int MASK_NEUTRAL = 2;
    
    public double radius;
    public int mask;
    
    
    public Hitcircle(double rad, int mask){
        super("circle");
        this.radius = rad;
        this.mask = mask;
    }
    
    public boolean effects(Hitcircle b){
        if(this.mask == MASK_PLAYER){
            switch(b.mask){
                case MASK_ENEMY:
                    return true;
                case MASK_NEUTRAL:
                    return true;
                case MASK_PLAYER:
                    return false;
                default:
                    return false;
            }
        }
        
        if(this.mask == MASK_ENEMY){
            switch(b.mask){
                case MASK_ENEMY:
                    return false;
                case MASK_NEUTRAL:
                    return true;
                case MASK_PLAYER:
                    return true;
                default:
                    return false;
            }
        }
        
        if(this.mask == MASK_NEUTRAL){
            switch(b.mask){
                case MASK_ENEMY:
                    return true;
                case MASK_NEUTRAL:
                    return false;
                case MASK_PLAYER:
                    return true;
                default:
                    return false;
            }
        }
        
        return false;
    }
    
    //THE DEFAULT BEHAVIOUR IS TO TREAT IT LIKE A SOLID OBJECT.
    public void collide(Entity owner, Entity collided){
        solid(owner, collided);
    }
    
    
    public void solid(Entity owner, Entity collided){
        try{
            Point vel = (Point)((Velocity)collided.getComponent("velocity")).vel;
            
            Point aPos = (Point)((Location)owner.getComponent("location")).loc;
            Point bPos = (Point)((Location)collided.getComponent("location")).loc;
            
            double angle = getAngle(aPos,bPos);
            
            double dist = vel.distance(new Point(0,0));
            
            double ySub = dist * Math.cos(angle);
            double xSub = dist * Math.cos(angle);
            
            
            if(aPos.x > bPos.x){
                xSub = xSub * -1;
            }
            
            if(aPos.y > bPos.y){
                ySub = ySub * -1;
            }
            
            vel.move((int)(vel.x - xSub),(int)(vel.y - ySub));
            
        } catch (Exception E){
            //It probably just doesn't have a velocity!
        }
    }
    
    private double getAngle(Point a, Point b) {
        double angle = (double) Math.toDegrees(Math.atan2(b.y - a.y, b.x - a.x));

        if(angle < 0){
            angle += 360;
        }

        return Math.toRadians(angle);
    }
    
    public void set(Component C){
        super.set(C);
        Hitcircle HC = (Hitcircle)C;
        this.mask = HC.mask;
        this.radius = HC.radius;
    }
    
    public Hitcircle clone(){
        return new Hitcircle(this.radius,this.mask);
    }
}
