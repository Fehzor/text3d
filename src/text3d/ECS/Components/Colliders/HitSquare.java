/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.ECS.Components.Colliders;

import java.awt.Point;
import text3d.ECS.Component;
import text3d.ECS.Components.Location;
import text3d.ECS.Components.Velocity;
import text3d.ECS.Entity;

/**
 *
 * @author FF6EB4
 */
public class HitSquare extends Component{
    
    public static final int MASK_PLAYER = 0;
    public static final int MASK_ENEMY = 1;
    public static final int MASK_NEUTRAL = 2;
    
    public int mask;
    
    
    public HitSquare(){
        super("square");
    }
    
    //THE DEFAULT BEHAVIOUR IS TO TREAT IT LIKE A SOLID OBJECT.
    public void collide(Entity owner, Entity collided){
        solidCube(owner, collided);
    }
    
    //COLLISION EFFECTS LISTED BELOW. OVER RIDE COLLIDE AND PUT THESE INTO IT TO BUILD SOMETHING.
    public void solidCube(Entity owner,Entity collided){
        try{
            Point vel = (Point)((Velocity)collided.getComponent("velocity")).vel;
            
            Point aPos = (Point)((Location)owner.getComponent("location")).loc;
            Point bPos = (Point)((Location)collided.getComponent("location")).loc;
            
            vel.x = 0;
            vel.y = 0;
            
        } catch (Exception E){
            //No velocity no problems.
        }
    }
    
    public void solid(Entity owner, Entity collided){
        try{
            Point vel = (Point)((Velocity)collided.getComponent("velocity")).vel;
            
            Point aPos = (Point)((Location)owner.getComponent("location")).loc;
            Point bPos = (Point)((Location)collided.getComponent("location")).loc;
            
            double angle = getAngle(aPos,bPos);
            
            double dist = vel.distance(new Point(0,0));
            
            double ySub = dist * Math.cos(angle);
            double xSub = dist * Math.cos(angle);
            
            
            if(aPos.x > bPos.x){
                xSub = xSub * -1;
            }
            
            if(aPos.y > bPos.y){
                ySub = ySub * -1;
            }
            
            vel.move((int)(vel.x - xSub),(int)(vel.y - ySub));
            
        } catch (Exception E){
            //It probably just doesn't have a velocity!
        }
    }
    
    private double getAngle(Point a, Point b) {
        double angle = (double) Math.toDegrees(Math.atan2(b.y - a.y, b.x - a.x));

        if(angle < 0){
            angle += 360;
        }

        return Math.toRadians(angle);
    }
    
    public void set(Component C){
        super.set(C);
        HitSquare HC = (HitSquare)C;
        this.mask = HC.mask;
    }
    
    public HitSquare clone(){
        return new HitSquare();
    }
}
