/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.ECS.Components;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import text3d.Display.ColorTuple;
import text3d.ECS.Component;

/**
 *
 * @author FF6EB4
 */
public class Acceleration extends Component{
    public Point acc;
    
    public Acceleration(int startX, int startY){
        super("acceleration");
        acc = new Point(startX,startY);
    }
    
    public int x(){
        return acc.x;
    }
    
    public int y(){
        return acc.y;
    }
    
    public Acceleration clone(){
        return new Acceleration(acc.x,acc.y);
    }
    
    public void set(Component C){
        super.set(C);
        this.acc.setLocation(((Acceleration)C).acc);
    }
}
