/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.ECS.Components;

import text3d.ECS.Component;
import text3d.ECS.Player.Player;

/**
 *
 * @author FF6EB4
 */
public class Angle extends Component{
    private double angle;
    public Angle(){
        super("angle");
        reuseable = false;
    }
    
    public double angle(){
        return angle;
    }
    
    public void setAngleDegrees(double degrees){
        this.angle = 2*Math.PI*degrees/360;
    }
    
    public void setAngle(double radians){
        this.angle = radians;
    }
    
    public void set(Component C){
        super.set(C);
        this.angle = ((Angle)C).angle;
    }
    
    public void setToPlayerAngle(){
        Angle P = (Angle) Player.thePlayer.getComponent("angle");
        
        this.angle = P.angle();
    }
}
