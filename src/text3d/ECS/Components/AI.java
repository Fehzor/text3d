/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.ECS.Components;

import text3d.ECS.Component;
import text3d.ECS.Entity;

/**
 *
 * @author FF6EB4
 */
public class AI extends Component{
    public AI(){
        super("intel");
    }
    
    public void act(Entity E){
        //Move to the right as the default behaviour.
        Velocity V = (Velocity)E.getComponent("velocity");
        V.vel.move(-3,0);
        
        try{
            Angle a = (Angle)E.getComponent("angle");
            a.setAngleDegrees(90);
        } catch (Exception Ex){}
    }
    
    public AI clone(){
        return new AI();
    }
}
