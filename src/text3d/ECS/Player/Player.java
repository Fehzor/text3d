/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.ECS.Player;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import text3d.Display.ColorTuple;
import text3d.ECS.Components.Angle;
import text3d.ECS.Components.Camera;
import text3d.ECS.Components.Colliders.Hitcircle;
import text3d.ECS.Components.Humanity;
import text3d.ECS.Components.Location;
import text3d.ECS.Components.Images.TextImage;
import text3d.ECS.Components.Images.TextImageAnimated;
import text3d.ECS.Components.Images.TextImageDualPaper;
import text3d.ECS.Components.Velocity;
import text3d.ECS.Entity;
import text3d.ECS.Player.Weapons.TestGun;
import text3d.Utility.ImageLoader;

/**
 *
 * @author FF6EB4
 */
public class Player extends Entity{
    public static Player thePlayer = new Player();
    
    public ArrayList<Weapon> weapons = new ArrayList<>();
    public Weapon current;
    
    private Player(){
        super();
        
        this.addComponent(new Location(0,0));
        this.addComponent(new Velocity(0,0));
        this.addComponent(new Humanity());
        
        Angle a = new Angle();
        this.addComponent(a);
        
        ImageLoader.switchMap("HUMAN");
        TextImageDualPaper TIDP = new TextImageDualPaper(a,"resources/Images/Player/player");
        
        /*
        ImageLoader.switchMap("GREYSCALE");
        TextImage sidea = ImageLoader.loadImage("resources/Images/Weapons/gun.txt");
        TextImage fronta = ImageLoader.loadImage("resources/Images/Weapons/gunfront.txt");
        TextImage backa = ImageLoader.loadImage("resources/Images/Weapons/gunback.txt");
        TextImageDualPaper TIDP2 = new TextImageDualPaper(a,fronta, sidea, backa);
        
        
        TIDP.registerSubImage(TIDP2, 4, -5, 0);
        */
        
        this.addComponent(TIDP);//☺☺☺
        
        this.addComponent(new Hitcircle(3,Hitcircle.MASK_PLAYER));
        
        this.addComponent(new Camera());
        
        this.addWeapon(new TestGun(a));
        
        //((TextImage)this.getComponent("image")).image.get(0).set(0, new ColorTuple(Color.PINK,Color.BLACK,';'));
        //((TextImage)this.getComponent("image")).image.get(0).add( new ColorTuple(Color.PINK,Color.BLACK,')'));
    }
    
    public void addWeapon(Weapon W){
        if(weapons.size() == 0){
            current = W;
        }
        weapons.add(W);
        ((TextImageDualPaper)this.getComponent("image")).registerSubImage(W.image, 4, -5, 0);
    }
}
