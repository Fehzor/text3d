/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.ECS.Player.Weapons.Obj;

import text3d.ECS.Blueprint;
import text3d.ECS.Components.Acceleration;
import text3d.ECS.Components.Angle;
import text3d.ECS.Components.Colliders.Hitcircle;
import static text3d.ECS.Components.Colliders.Hitcircle.MASK_NEUTRAL;
import static text3d.ECS.Components.Colliders.Hitcircle.MASK_PLAYER;
import text3d.ECS.Components.Images.TextImage;
import text3d.ECS.Components.Images.TextImageDualPaper;
import text3d.ECS.Components.Images.TextImageReuseable;
import text3d.ECS.Components.Location;
import text3d.ECS.Components.Velocity;
import text3d.ECS.Player.Player;
import text3d.Utility.ImageLoader;

/**
 *
 * @author FF6EB4
 */
public class TestBullet extends Blueprint{
    public TestBullet(){
        this.components.add(new Location(0,0));
        Angle a = new Angle();
        this.components.add(new Velocity(0,0));
        this.components.add(new Acceleration(0,0));
        
        TextImageReuseable TIR = new TextImageReuseable(ImageLoader.loadImage("resources/Images/Weapons/testGun/bullet.txt"));
                //(a,"resources/Images/Player/bullet");
        this.components.add(TIR);
        this.components.add(new Hitcircle(1,MASK_PLAYER));
    }
}
