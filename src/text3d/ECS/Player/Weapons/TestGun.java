/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.ECS.Player.Weapons;

import text3d.Display.TextDisplay;
import text3d.ECS.Blueprint;
import text3d.ECS.Components.Acceleration;
import text3d.ECS.Components.Angle;
import text3d.ECS.Components.Images.TextImageDualPaper;
import text3d.ECS.Components.Location;
import text3d.ECS.Components.Velocity;
import text3d.ECS.ECSHandler;
import text3d.ECS.Entity;
import text3d.ECS.Player.Player;
import text3d.ECS.Player.Weapon;
import text3d.Utility.ImageLoader;

/**
 *
 * @author FF6EB4
 */
public class TestGun extends Weapon{
    public TestGun(Angle ang){
        ImageLoader.switchMap("GREYSCALE");
        this.image = new TextImageDualPaper(ang,"resources/Images/Weapons/testGun/gun");
    }
    
    public void act(){
        Entity boolet = ECSHandler.build(Blueprint.prints.get("testbullet"));
        
        Angle a = (Angle) boolet.getComponent("angle");
        Location l = (Location) boolet.getComponent("location");
        Acceleration ac = (Acceleration) boolet.getComponent("acceleration");
        
        a.setToPlayerAngle();
        l.setToPlayerLocation();
        l.loc.move((int) (l.loc.x+Math.sin((double)a.angle())*-8), l.loc.y - 6);
        
        double accX = ((double)Math.sin((double)a.angle()) * (double)-5);
        double accY = ((double)Math.cos((double)a.angle()) * (double)-5);
        
        ac.acc.move((int)accX,(int)accY);
        
        ECSHandler.checkEntity(boolet);
        TextDisplay.currentBlock.checkEntity(boolet);
    }
}
