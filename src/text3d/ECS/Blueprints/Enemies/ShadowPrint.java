/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.ECS.Blueprints.Enemies;

import text3d.ECS.Blueprint;
import text3d.ECS.Components.*;
import text3d.ECS.Components.Images.TextImageDualPaper;
import text3d.Utility.ImageLoader;

/**
 *
 * @author FF6EB4
 */
public class ShadowPrint extends Blueprint{
    public ShadowPrint(){
        this.components.add(new Location(6,6));
        this.components.add(new Velocity(0,0));
        Angle a = new Angle();
        this.components.add(a);
        ImageLoader.switchMap("GREYSCALE");
        this.components.add(new TextImageDualPaper(a,"resources/Images/Enemies/Shadow/shadow"));
        
        this.components.add(new AI());
    }
}
