/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text3d.ECS.Blueprints.Cubes;

import text3d.ECS.Blueprint;
import text3d.ECS.Components.Colliders.HitSquare;
import text3d.ECS.Components.Colliders.Hitcircle;
import static text3d.ECS.Components.Colliders.Hitcircle.MASK_NEUTRAL;
import text3d.ECS.Components.Images.TextImage;
import text3d.ECS.Components.Images.TextImageReuseable;
import text3d.ECS.Components.Location;
import text3d.Utility.ImageLoader;

/**
 *
 * @author FF6EB4
 */
public class BasicCube extends Blueprint{
    public BasicCube(){
        this.components.add(new Location(10,10));
        TextImage TI = ImageLoader.loadImage("resources/Images/Cubes/cube.txt");
        TextImageReuseable TIR = new TextImageReuseable(TI);
        this.components.add(TI);
        this.components.add(new HitSquare());
    }
}
